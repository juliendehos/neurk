.PHONY: all clean

all:
	make -C neurk_cpp
	make -C neurk_eigen
	make -C nn42h

clean:
	make -C neurk_cpp clean
	make -C neurk_eigen clean
	make -C nn42h clean

