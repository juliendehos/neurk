{-
Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING.WTFPL file for more details.
-}

{- example from http://www-lisic.univ-littoral.fr/~teytaud/ -}

import Neurk

main = do
    let neurons = [(0, 1.0, 0.0, 0.0), (1, 1.0, 0.0, 0.0), (2, 1.0, 0.0, 0.0), (3, 1.0, 0.0, 0.0), (4, 1.0, 0.0, 0.0), (5, 1.0, 0.0, 0.0)]
        synapses = [(0, 3, 0.2), (0, 4, -0.3), (0, 5, 0.4), (1, 3, 0.1), (1, 4, -0.2), (2, 3, 0.3), (2, 4, 0.4), (3, 5, 0.5), (4, 5, -0.4)]
        inputs = [0, 1, 2]
        outputs = [5]
        network0 = (neurons, synapses, inputs, outputs)
        network1 = computeEvaluation [1.0, 1.0, 1.0] network0
        network2 = computeTraining [0.0] 1.0 network1
        network3 = computeEvaluation [1.0, 1.0, 1.0] network2
    putStrLn $ printNetwork network0
    putStrLn $ printNetwork network1
    putStrLn $ printNetwork network2
    putStrLn $ printNetwork network3

