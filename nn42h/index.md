---
title: "Neural network in 42 lines of Haskell"
date: 2016-06-22
---

This page presents an implementation of neural networks in Haskell. 
This implementation is straightforward but computationally inefficient. 

See also: [The Monad.Reader Issue 21](http://themonadreader.files.wordpress.com/2013/03/issue214.pdf)

# Neural networks 

## Representation 

A neural network can be represented by a set of neurons interconnected with directed synapses.
A neuron $j$ has a function parameter $\lambda_j$, an output value $y_j$ and a training error $\delta_j$.
It can be connected to a source neuron $i$ with a synapse of weight $w_{i,j}$ and connected to a destination neuron $k$ with a synapse of weight $w_{j,k}$.
A neuron with no source neuron is an input neuron.
A neuron with no destination neuron is an output neuron.

For example, the following neural network has 6 neurons and 9 synapses.
The neurons 1, 2 and 3 are input neurons.
The neuron 6 is an output neuron.

![](nn1.svg)

## Evaluation

To evaluate a network (i.e. compute its result for a data sample (input values)), we first initialize the input neurons to the given input values:
$$ y_j = \text{input value for } j $$
Then we recursively compute all destination neurons with:
$$ y_j = \frac{1}{1 + e^{-\lambda_j \sigma_j}} $$
where
$$\sigma_j = \sum_{i \in \text{src}(j)} y_i w_{i,j}$$
Finally, the result can be read in the output neurons ($y_j$).

## Training

To train a network (i.e. adjust synapse weights using a training sample (input values $y_j$ and expected outputs $u_j$), we first evaluate the network with the given inputs.  Then we compute the error of each neuron, backward. For output neurons, the error is:
$$\delta_j =  y_j (1 - y_j)(u_j - y_j) $$
and for source neurons, recursively:
$$\delta_j = y_j (1 - y_j) \sum_{k \in \text{dst}(j)} \delta_k w_{j,k} $$
Finally, we adjust the synapse weights with:
$$w'_{i,j} =  w_{i,j} + \alpha y_i \delta_j $$
where $\alpha$ is the training parameter.

# Implementation 

## Data types

Using classic Haskell data structures (tuple, list), we define three types to represent a neural network:

```haskell
type Neuron = (Int, Double, Double, Double)         -- index, parameter (lambda), value (y), error (sigma)
type Synapse = (Int, Int, Double)                   -- input index, output index, weight (w)
type Network = ([Neuron], [Synapse], [Int], [Int])  -- neurons, synapses, input neuron indices, output neuron indices 
```

## Propagation 

We need to propagate values in a network, in both directions: from input neurons to output neurons (network evaluation) and from output neurons to input neurons (network training).

First, we need two functions to find the source neurons and the destination neurons of a given neuron.
In fact, it is more convenient, for our propagation problem, that these functions find source/destination neurons of a list of neurons (<var>indices</var>).
Here, we use a list comprehension that filters the synapse list (<var>synapses</var>):

```haskell
import Data.List  - for nub (remove duplicate elements) 
getSrcIndices :: [Int] -> [Synapse] -> [Int]
getSrcIndices indices synapses = nub [i | (i, j, _) <- synapses, j `elem` indices]
getDstIndices :: [Int] -> [Synapse] -> [Int]
getDstIndices indices synapses = nub [k | (j, k, _) <- synapses, j `elem` indices] 
```

We also need to apply a function to each neuron of a list. To do that, we traverse the whole neuron list of the network (<var>neurons</var>) and apply the neuron function (<var>neuronFunc</var>) to the specified neurons (<var>indices</var>) only:

```haskell
computeLayer :: [Int] -> [Neuron] -> [Synapse] -> (Neuron->[Neuron]->[Synapse]->Neuron) -> [Neuron]
computeLayer indices neurons synapses neuronFunc = map (\n@(j,_,_,_) -> if j `elem` indices then neuronFunc n neurons synapses else n) neurons
```

To implement the propagation function, we apply the neuron function (<var>neuronFunc</var>) to a list of neurons (<var>indices</var>) using <var>computeLayer</var> and find (using <var>layerFunc</var>) the neurons to update in the next recursion of the propagation.

```haskell
propagate :: [Int] -> [Neuron] -> [Synapse] -> ([Int]->[Synapse]->[Int]) -> (Neuron->[Neuron]->[Synapse]->Neuron) -> [Neuron]
propagate [] neurons _ _ _ = neurons
propagate indices neurons synapses layerFunc neuronFunc = propagate indices' neurons' synapses layerFunc neuronFunc
    where indices' = layerFunc indices synapses 
          neurons' = computeLayer indices neurons synapses neuronFunc
```

Thus, the <var>propagate</var> function can perform forward propagation using <var>getDstIndices</var> as the <var>layerFunc</var> parameter, and backward propagation using <var>getSrcIndices</var>.

## Evaluation

To evaluate a network, we first have to initialize the value of input neurons ($y_j$) to the given input values. To this end, we use a list of tuples (index, value) and we define a function which initialize one neuron (<var>neuron</var>) if this neuron is in the list of tuples (<var>jys</var>).

```haskell
initNeuronEvaluation :: [(Int, Double)] -> Neuron -> Neuron
initNeuronEvaluation jys neuron@(j, lambda, _, delta) = if null ys then neuron else (j, lambda, head ys, delta)
    where ys = [y' | (j', y') <- jys, j == j'] 
```

We define a function to evaluate a neuron (see the formula above).
This function need to access to the source neurons ($y_i$) and to the source synapses ($w_{i,j}$).
This is quite easy to code (and costly to execute) using list comprehension with both the neuron list and the synapse list.

```haskell
evaluateNeuron :: Neuron -> [Neuron] -> [Synapse] -> Neuron
evaluateNeuron (j, lambda, _, delta) neurons synapses = (j, lambda, y', delta)
    where sigma = sum [y*w | (i, _, y, _) <- neurons, (i', j', w) <- synapses, i == i' && j' == j]
          y' = 1.0 / (1.0 + exp (- lambda * sigma))
```

Now, we can define a function to evaluate a network for a given data sample (<var>inputValues</var>).
We first initialize the input neurons using <var>initNeuronEvaluation</var>
and find the first neurons to consider in the propagation (the destination neurons of the input neurons).
Finally, we call <var>propagate</var> to apply <var>evaluateNeuron</var> to the destination neurons(using <var>getDstIndices</var>) recursively. 

```haskell
computeEvaluation ::[Double] ->  Network -> Network
computeEvaluation inputValues (neurons, synapses, inputIndices, outputIndices) = (neurons', synapses, inputIndices, outputIndices)
    where initNeurons = map (initNeuronEvaluation (zip inputIndices inputValues)) neurons
          layerIndices = getDstIndices inputIndices synapses
          neurons' = propagate layerIndices initNeurons synapses getDstIndices evaluateNeuron
```

## Training 

In this implementation, <var>computeEvaluation</var> should be called before each training. 

We define a function to initialize the output error, i.e. compute the error ($\delta_j$) between its previously computed value ($y_j$) and its expected value ($u_j$). This function uses a list (<var>jus</var>) of tuples (index, expected value). 

```haskell
initNeuronTraining :: [(Int, Double)] -> Neuron -> Neuron
initNeuronTraining jus neuron@(j, lambda, y, _) = if null deltas then neuron else (j, lambda, y, head deltas)
    where deltas = [y*(1-y)*(u-y) | (j', u) <- jus, j == j']
```

Then we define a function to compute the training error of a non-output neuron, using its destination neurons.

```haskell
trainNeuron :: Neuron -> [Neuron] -> [Synapse] -> Neuron
trainNeuron (j, lambda, y, _) neurons synapses = (j, lambda, y, delta')
    where delta' = y*(1-y)*sum [deltak*wjk | (sj, sk, wjk) <- synapses, (nk, _, _, deltak) <- neurons, sj == j && nk == sk]
```

After the training error is computed for all neurons, we can update the synapse weights. 
Here again, we use a list comprehension with both the neuron list and the synapse list.

```haskell
trainSynapse :: [Neuron] -> Double -> Synapse -> Synapse
trainSynapse neurons alpha (i, j, w) = (i, j, w')
    where w' = w + head [alpha*yi*deltaj | (ni, _, yi, _) <- neurons, (nj, _, _, deltaj) <- neurons, ni == i && nj == j]
```

Putting it all together, we define a function to compute training from a training sample, assuming the evaluation for this sample is already computed.
To compute the training errors, we just have to initialize output neurons with <var>initNeuronTraining</var>, find the first neurons to propagate with <var>getSrcIndices</var> and update the network backward with <var>propagate</var>.
Finally, we call <var>trainSynapse</var> to compute the new synapse weights from the computed training errors.

```haskell
computeTraining :: [Double] -> Double -> Network -> Network
computeTraining expectedValues alpha (neurons, synapses, inputIndices, outputIndices) = (neurons', synapses', inputIndices, outputIndices)
    where initNeurons = map (initNeuronTraining (zip outputIndices expectedValues)) neurons
          layerIndices = getSrcIndices outputIndices synapses
          neurons' = propagate layerIndices initNeurons synapses getSrcIndices trainNeuron
          synapses' = map (trainSynapse neurons' alpha) synapses
```

## General network 

With the previous code, we can implement any neural network. For example (same network as in the beginning of this page, with $\lambda_j = 1$ and $\alpha = 1$):

![](nn2.svg)

```haskell
main = do
    let neurons = [(1, 1.0, 0.0, 0.0), (2, 1.0, 0.0, 0.0), (3, 1.0, 0.0, 0.0), (4, 1.0, 0.0, 0.0), (5, 1.0, 0.0, 0.0), (6, 1.0, 0.0, 0.0)]
        synapses = [(1, 4, 0.2), (1, 5, -0.3), (1, 6, 0.4), (2, 4, 0.1), (2, 5, -0.2), (3, 4, 0.3), (3, 5, 0.4), (4, 6, 0.5), (5, 6, -0.4)]
        inputs = [1, 2, 3]
        outputs = [6]
        network0 = (neurons, synapses, inputs, outputs)
        network1 = computeEvaluation [1.0, 1.0, 1.0] network0
        network2 = computeTraining [0.0] 1.0 network1
        network3 = computeEvaluation [1.0, 1.0, 1.0] network2
    putStrLn $ "initial network\n" ++ show network0
    putStrLn $ "evaluation\n" ++ show network1
    putStrLn $ "training\n" ++ show network2
    putStrLn $ "evaluation\n" ++ show network3 
```

## Multi-layer network 

It can be interesting to build multi-layer networks in which all neurons of a layer are only connected to all neurons of the previous layer and to all neurons of the next layer.
For example, a multi-layer network with 4 input neurons and 2 output neurons:

![](nn3.svg)

To create a multi-layer more easily, we label the neurons, starting from 1, layer-by-layer, from the output neuron to the input neurons. 
If the network has more than one output, we avoid the corresponding labels.
This makes the creation of the synapses very simple since we know that a layer $l$ corresponds to the neurons $\frac{l(l-1)}{2} + 1$ to $\frac{l(l+1)}{2}$. 

We define a function to create a multi-layer network, given the number of input neurons, the number of output neurons and the default values for the neuron parameters (<var>lambda</var>) and for the synapse weights (<var>w</var>).
We may notice that list comprehension makes the creation of the synapses really easy to write.

```haskell
createMultilayerNetwork :: Int -> Int -> Double -> Double -> Network
createMultilayerNetwork nbInputs nbOutputs lambda w = (neurons, synapses, inputIndices, outputIndices)
    where indA l = l*(l-1) `div` 2 + 1
          indB l = l*(l+1) `div` 2
          neurons = [(i, lambda, 0.0, 0.0) | i <- [(indA nbOutputs) .. (indB nbInputs)]]
          synapses = [(i,j,w) | l<-[(nbOutputs+1)..nbInputs], i<-[(indA l)..(indB l)], j<-[(indA (l-1))..(indB (l-1))]]
          inputIndices = [(indA nbInputs) .. (indB nbInputs)]
          outputIndices = [(indA nbOutputs) .. (indB nbOutputs)]
```

We also define a function to set the parameter of a neuron to a given value:

```haskell
setNeuronParameter :: Int -> Double -> Network -> Network
setNeuronParameter j lambda (neurons, synapses, inputIndices, outputIndices) = (neurons', synapses, inputIndices, outputIndices)
    where neurons' = map (\ n@(nj, _, ny, ndelta) -> if nj == j then (nj, lambda, ny, ndelta) else n) neurons
```

and a function to set the weight of a synapse:

```haskell
setSynapseWeight :: Int -> Int -> Double -> Network -> Network
setSynapseWeight i j w (neurons, synapses, inputIndices, outputIndices) = (neurons, synapses', inputIndices, outputIndices)
    where synapses' = map (\ s@(si, sj, _) -> if si == i && sj == j then (si, sj, w) else s) synapses
```

An example of multi-layer network with 4 input neurons and 2 output neurons:

```haskell
main' = do
    let network0 = setNeuronParameter 9 0.5 (setSynapseWeight 5 3 0.2 (createMultilayerNetwork 4 2 1.0 1.0))
        network1 = computeEvaluation [1.0, 1.0, 1.0, 1.0] network0
        network2 = computeTraining [0.0, 1.0] 1.0 network1
        network3 = computeEvaluation [1.0, 1.0, 1.0, 1.0] network2
    putStrLn $ "initial network\n" ++ show network0
    putStrLn $ "evaluation\n" ++ show network1
    putStrLn $ "training\n" ++ show network2
    putStrLn $ "evaluation\n" ++ show network3 
```

