{-
Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING.WTFPL file for more details.
-}

{- main (multi-layer 4 inputs 2 outputs) -}

import Neurk

main = do
    let network0 = setNeuronParameter 9 0.5 (setSynapseWeight 5 3 0.2 (createMultilayerNetwork 4 2 1.0 1.0))
        network1 = computeEvaluation [1.0, 1.0, 1.0, 1.0] network0
        network2 = computeTraining [0.0, 1.0] 1.0 network1
        network3 = computeEvaluation [1.0, 1.0, 1.0, 1.0] network2
    putStrLn $ printNetwork network0
    putStrLn $ printNetwork network1
    putStrLn $ printNetwork network2
    putStrLn $ printNetwork network3

