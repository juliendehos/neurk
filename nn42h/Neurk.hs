{-
Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING.WTFPL file for more details.
-}

module Neurk ( Neuron, Synapse, Network, printNetwork,
    createMultilayerNetwork, setNeuronParameter, setSynapseWeight,
    computeTraining, computeEvaluation )
where

import Data.List  -- nub

-- key, parameter, value, error
type Neuron = (Int, Double, Double, Double)

-- input neuron, output neuron, weight
type Synapse = (Int, Int, Double)

-- neurons, synapses, inputs, outputs
type Network = ([Neuron], [Synapse], [Int], [Int])

{-
multi-layer network, e.g. 4 inputs 2 outputs :
7 8 9 10  (inputs)
 4 5 6
  2 3     (outputs)
-}
createMultilayerNetwork :: Int -> Int -> Double -> Double -> Network
createMultilayerNetwork ni no p w = (n, s, i, o)
    where indA l = l*(l-1) `div` 2 + 1
          indB l = l*(l+1) `div` 2
          n = [(i, p, 0.0, 0.0) | i <- [(indA no) .. (indB ni)]]
          s = [(i,j,w) | l<-[(no+1)..ni], i<-[(indA l)..(indB l)], j<-[(indA (l-1))..(indB (l-1))]]
          i = [(indA ni) .. (indB ni)]
          o = [(indA no) .. (indB no)] 

setNeuronParameter :: Int -> Double -> Network -> Network
setNeuronParameter j p (rn, rs, ri, ro) = (rn', rs, ri, ro)
    where rn' = map (\ n@(nj, _, nv, ne) -> if nj == j then (nj, p, nv, ne) else n) rn

setSynapseWeight :: Int -> Int -> Double -> Network -> Network
setSynapseWeight i j w (rn, rs, ri, ro) = (rn, rs', ri, ro)
    where rs' = map (\ s@(si, sj, _) -> if si == i && sj == j then (si, sj, w) else s) rs

printNetwork :: Network -> String
printNetwork (n, s, i, o) = "Neurons: " ++ show n ++ "\nSynapses: " ++ show s ++ "\nInputs: " ++ show i ++ "\nOutputs: " ++ show o

getInputKeys :: [Int] -> [Synapse] -> [Int]
getInputKeys keys s = nub [i | (i, j, _) <- s, j `elem` keys]

getOutputKeys :: [Int] -> [Synapse] -> [Int]
getOutputKeys keys s = nub [j | (i, j, _) <- s, i `elem` keys]

-- apply a function to neurons with given keys
computeLayer :: [Int] -> [Neuron] -> [Synapse] -> (Neuron -> [Neuron] -> [Synapse] -> Neuron) -> [Neuron]
computeLayer keys n s neuronFunc = map (\x@(k, _, _, _) -> if k `elem` keys then neuronFunc x n s else x) n

-- apply a function to successive sets of neurons
propagate :: [Int] -> [Neuron] -> [Synapse] -> ([Int] -> [Synapse] -> [Int]) -> (Neuron -> [Neuron] -> [Synapse] -> Neuron) -> [Neuron]
propagate [] n _ _ _ = n
propagate k n s layerFunc neuronFunc = propagate k' n' s layerFunc neuronFunc
    where k' = layerFunc k s 
          n' = computeLayer k n s neuronFunc 
 
{- evaluation -}

initNeuronEvaluation :: [(Int, Double)] -> Neuron -> Neuron
initNeuronEvaluation zs n@(k, p, _, e) = if null z then n else (k, p, head z, e)
    where z = [v' | (k', v') <- zs, k == k']

evaluateNeuron :: Neuron -> [Neuron] -> [Synapse] -> Neuron
evaluateNeuron (k, p, _, e) n s = (k, p, v', e)
    where inputValues = [v*w | (i, _, v, _) <- n, (i', j', w) <- s, i == i' && j' == k]
          v' = 1.0 / (1.0 + exp (- p * sum inputValues))

computeEvaluation ::[Double] ->  Network -> Network
computeEvaluation inputValues (n, s, i, o) = (n', s, i, o)
    where initNeurons = map (initNeuronEvaluation (zip i inputValues)) n
          keys = getOutputKeys i s
          n' = propagate keys initNeurons s getOutputKeys evaluateNeuron

{- training (you have to call computeEvaluation first) -}

initNeuronTraining :: [(Int, Double)] -> Neuron -> Neuron
initNeuronTraining zs n@(k, p, v, _) = if null z then n else (k, p, v, head z)
    where z = [(v'-v)*v*(1-v) | (k', v') <- zs, k == k']

trainNeuron :: Neuron -> [Neuron] -> [Synapse] -> Neuron
trainNeuron (j, p, v, _) n s = (j, p, v, e')
    where inputs = [sw*ne | (sj, sk, sw) <- s, (nk, _, _, ne) <- n, sj == j && nk == sk]
          e' = v * (1-v) * sum inputs

trainSynapse :: [Neuron] -> Double -> Synapse -> Synapse
trainSynapse n alpha (i, j, w) = (i, j, w')
    where z = [alpha*ej*vi | (ni, _, vi, _) <- n, (nj, _, _, ej) <- n, ni == i && nj == j]
          w' = w + head z

computeTraining :: [Double] -> Double -> Network -> Network
computeTraining outputValues alpha (n, s, i, o) = (n', s', i, o)
    where initNeurons = map (initNeuronTraining (zip o outputValues)) n
          keys = getInputKeys o s
          n' = propagate keys initNeurons s getInputKeys trainNeuron
          s' = map (trainSynapse n' alpha) s

