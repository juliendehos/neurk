# neurk_eigen

C++ implementation using the eigen library

## How to run the examples

Make sure the [eigen](http://eigen.tuxfamily.org) library is installed.

```
make
./bin/example1.out
./bin/example2.out
```

