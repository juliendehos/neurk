// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "neurk.hpp"

// example from http://www-lisic.univ-littoral.fr/~teytaud/
int main(int, char **)
{ 
    // create network
    Network n;
    n.createNetwork(6);

    // init neurons
    n.initNeuron(0, 0.0);
    n.initNeuron(1, 0.0);
    n.initNeuron(2, 0.0);
    n.initNeuron(3, 1.0);
    n.initNeuron(4, 1.0);
    n.initNeuron(5, 1.0);

    // init synapses
    n.initSynapse(0, 3, 0.2);
    n.initSynapse(0, 4, -0.3);
    n.initSynapse(0, 5, 0.4);
    n.initSynapse(1, 3, 0.1);
    n.initSynapse(1, 4, -0.2);
    n.initSynapse(2, 3, 0.3);
    n.initSynapse(2, 4, 0.4);
    n.initSynapse(3, 5, 0.5);
    n.initSynapse(4, 5, -0.4);

    // init input/output
    n.addInput(0);
    n.addInput(1);
    n.addInput(2);
    n.addOutput(5);

    // evaluation
    std::cout << "------------ evaluation ------------\n";
    n.computeEvaluation({1, 1, 1});
    std::cout << n << std::endl;

    // training
    std::cout << "------------ training ------------\n";
    n.computeTraining(1, {1, 1, 1}, {0});
    std::cout << n << std::endl;

    // evaluation
    std::cout << "------------ evaluation ------------\n";
    n.computeEvaluation({1, 1, 1});
    std::cout << n << std::endl;

    return 0;
}

