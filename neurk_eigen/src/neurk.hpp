// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include <Eigen/Dense>
#include <iostream>
#include <vector>

class Network
{
    int _nbNeurons;

    Eigen::VectorXd _neuronValues;
    Eigen::VectorXd _neuronParameters;

    std::vector<int> _inputNeurons;
    std::vector<int> _outputNeurons;

    Eigen::MatrixXd _synapseWeights;
    Eigen::MatrixXi _synapseConnections;
    
    public:

    void createNetwork(int nbNeurons);
    void createMultiLayer(int M, int N, double weight, double parameter);

    void initNeuron(int i, double parameter);
    void initSynapse(int i, int j, double weight);

    void addInput(int i);
    void addOutput(int i);

    // (parameter, value) of each neuron
    std::vector<std::tuple<double,double>> getNeurons() const;
    // (from neuron, to neuron, weight) of each synapse
    std::vector<std::tuple<int,int,double>> getSynapses() const;
    // (neuron, value) of each output neuron
    std::vector<std::tuple<int,double>> getOutputs() const;

    void computeEvaluation(const std::vector<double> & data);
    void computeTraining(double alpha, 
            const std::vector<double> & data,
            const std::vector<double> & knowledge);

    private:

    double sigmoid(double parameter, double x) const;
    void computeLocalEvaluation(int j);
    void computeLocalOutputTraining(int j, double knowledge, std::vector<double> & errors); 
    void computeLocalHiddenTraining(int j, std::vector<double> & errors); 
    std::vector<int> findInputNeurons(int j) const;
    std::vector<int> findOutputNeurons(int j) const;

    friend std::ostream & operator<<(std::ostream & os, const Network & n);
};

std::ostream & operator<<(std::ostream & os, const Network & n);

