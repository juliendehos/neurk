// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "neurk.hpp"

#include <set>

std::ostream & operator<<(std::ostream & os, const Network & n)
{
    os << "neuron; parameter; value \n";
    std::vector<std::tuple<double,double>> vn = n.getNeurons();
    for (unsigned i=0; i<vn.size(); i++)
        os << "  " << i << "; " << std::get<0>(vn[i]) << "; " << std::get<1>(vn[i]) << std::endl;

    os << "synapse; from neuron; to neuron; weight\n";
    std::vector<std::tuple<int,int,double>> vs = n.getSynapses();
    for (unsigned i=0; i<vs.size(); i++)
        os << "  " << i << "; " << std::get<0>(vs[i]) << "; " << std::get<1>(vs[i]) << "; " << std::get<2>(vs[i]) << std::endl;

    os << "output neuron; value\n";
    for (auto o : n.getOutputs())
        os << "  " << std::get<0>(o) << "; " << std::get<1>(o) << std::endl;

    return os;
}

void Network::createNetwork(int nbNeurons)
{
    _nbNeurons = nbNeurons;

    _neuronValues.resize(nbNeurons);
    _neuronValues.setZero();

    _neuronParameters.resize(nbNeurons);
    _neuronParameters.setZero();

    _inputNeurons.clear();

    _outputNeurons.clear();

    _synapseWeights.resize(nbNeurons, nbNeurons);
    _synapseWeights.setZero();

    _synapseConnections.resize(nbNeurons, nbNeurons);
    _synapseConnections.setZero();
    
}

void Network::initNeuron(int i, double parameter)
{
    _neuronParameters(i) = parameter;
}

void Network::initSynapse(int i, int j, double weight)
{
    _synapseWeights(i,j) = weight;
    _synapseConnections(i,j) = 1;
}

void Network::addInput(int i)
{
    _inputNeurons.push_back(i);
}

void Network::addOutput(int i)
{
    _outputNeurons.push_back(i);
}

std::vector<std::tuple<double,double>> Network::getNeurons() const
{
    std::vector<std::tuple<double,double>> v;
    for (unsigned i=0; i<_neuronValues.size(); i++)
    {
        v.push_back(std::make_tuple(_neuronParameters(i), _neuronValues(i)));
    }
    return v;
}

std::vector<std::tuple<int,int,double>> Network::getSynapses() const
{
    std::vector<std::tuple<int,int,double>> v;
    for (int i=0; i<_synapseConnections.rows(); i++)
    {
        for (int j=0; j<_synapseConnections.cols(); j++)
        {
            if (_synapseConnections(i,j) != 0)
            {
                v.push_back(std::make_tuple(i, j, _synapseWeights(i,j)));
            }
        }
    }
    return v;
}

std::vector<std::tuple<int,double>> Network::getOutputs() const
{
    std::vector<std::tuple<int,double>> v;
    for (unsigned k=0; k<_outputNeurons.size(); k++)
    {
        int i = _outputNeurons[k];
        v.push_back(std::make_tuple(i, _neuronValues(i)));
    }
    return v;
}

void Network::computeEvaluation(const std::vector<double> & data)
{
    assert(_inputNeurons.size() == data.size());

    std::set<int> set1;
    std::set<int> set2;
    std::set<int> * ptrCurrentSet = &set1;
    std::set<int> * ptrNextSet = &set2;
    
    // for each input neuron
    for (int j : _inputNeurons)
    {
        // put data into the neuron
        _neuronValues(j) = data[j];

        // remember the neuron has to be updated
        ptrCurrentSet->insert(j);
    }

    // update neurons using multiple passes
    // from input neurons to output neurons
    while (not ptrCurrentSet->empty())
    {
        // for each neuron of the set to update
        for (int j : *ptrCurrentSet)
        {
            // update the neuron value 
            computeLocalEvaluation(j);

            // get output neurons for the next updating pass
            for (int k=0; k<_synapseConnections.cols(); k++)
            {
                if (_synapseConnections(j,k) != 0)
                {
                    ptrNextSet->insert(k);
                }
            }
        }

        // go to next updating pass
        std::swap(ptrCurrentSet, ptrNextSet);
        ptrNextSet->clear();
    }
}

void Network::computeTraining(double alpha, 
        const std::vector<double> & data,
        const std::vector<double> & knowledge)
{
    assert(_outputNeurons.size() == knowledge.size());

    // evaluate network
    computeEvaluation(data);

    // compute training errors of all neurons
    std::set<int> set1;
    std::set<int> set2;
    std::set<int> * ptrCurrentSet = &set1;
    std::set<int> * ptrNextSet = &set2;
    std::vector<double> errors(_neuronValues.size(), 0.0);
 
    // compute local training for output neurons
    for (unsigned x=0; x<_outputNeurons.size(); x++)
    {
        int j=_outputNeurons[x];
        computeLocalOutputTraining(j, knowledge[x], errors);
        ptrCurrentSet->insert(j);
    }

    // update neurons using multiple passes
    // from output neurons to input neurons
    while (not ptrCurrentSet->empty())
    {
        // for each neuron of the set to update
        for (int j : *ptrCurrentSet)
        {
            // update error for j
            computeLocalHiddenTraining(j, errors);

            // get input neurons for the next updating pass
            for (int i=0; i<_synapseConnections.rows(); i++)
            {
                if (_synapseConnections(i,j) != 0)
                {
                    ptrNextSet->insert(i);
                }
            }
        }

        // go to next updating pass
        std::swap(ptrCurrentSet, ptrNextSet);
        ptrNextSet->clear();
    }

    // update synapse weights
    for (int i=0; i<_synapseWeights.rows(); i++)
    {
        for (int j=0; j<_synapseWeights.cols(); j++)
        {
            if (_synapseConnections(i,j) != 0)
            {
                _synapseWeights(i,j) += alpha * errors[j] * _neuronValues(i);
            }
        }
    }
}

double Network::sigmoid(double parameter, double x) const
{
    return 1.0 / ( 1.0 + exp(- x*parameter) );
}

void Network::computeLocalEvaluation(int j)
{
    // find input neurons
    std::vector<int> inputs = findInputNeurons(j);
    // if no input neuron, it is a data neuron and nothing to do
    // else update output value from input neurons
    if (not inputs.empty())
    {
        double sigma = 0;
        for (int i : inputs)
        {
            sigma += _neuronValues(i) * _synapseWeights(i,j);
        }
        _neuronValues(j) = sigmoid(_neuronParameters(j), sigma);
    }
}

void Network::computeLocalOutputTraining(int j, double knowledge, std::vector<double> & errors) 
{
    double value = _neuronValues(j);
    errors[j] = (knowledge - value) * value * (1 - value);
}

void Network::computeLocalHiddenTraining(int j, std::vector<double> & errors) 
{
    std::vector<int> outputs = findOutputNeurons(j);
    if (not outputs.empty())
    {
        // compute local error
        double v = 0;
        for (int k : outputs)
        {
            v += errors[k] * _synapseWeights(j,k);
        }
        errors[j] = v * _neuronValues(j) * (1 - _neuronValues(j));
    }
}

std::vector<int> Network::findInputNeurons(int j) const
{
    std::vector<int> inputs;
    for (unsigned i=0; i<_synapseConnections.rows(); i++)
    {
        if (_synapseConnections(i,j) != 0)
        {
            inputs.push_back(i);
        }
    }
    return inputs;
}

std::vector<int> Network::findOutputNeurons(int j) const
{
    std::vector<int> outputs;
    for (unsigned k=0; k<_synapseConnections.cols(); k++)
    {
        if (_synapseConnections(j,k) != 0)
        {
            outputs.push_back(k);
        }
    }
    return outputs;
}

void Network::createMultiLayer(int M, int N, double weight, double parameter)
{
    int nbNeurons = (M+N)*(M-N+1)/2;
    createNetwork(nbNeurons);
    
    // init neurons
    _neuronParameters.fill(parameter);

    // init synapses
    // for all layer
    for (int i=M,k=0; i>N; i--)
    {
        // select a neuron of this main layer
        for (int j=0; j<i; j++)
        {
            // add synapses from neurons of the main layer to all neurons of the sub-layer
            for (int l=0; l<i-1; l++)
            {
                initSynapse(k+j, k+i+l, weight);
            }
        }
        k += i;
    }

    // input neurons
    for (int i=0; i<M; i++)
    {
        addInput(i);
    }

    // output neurons
    for (int i=0; i<N; i++)
    {
        addOutput(nbNeurons - N + i);
    }
}

