# NEUral netwoRK 
[![Build status](https://gitlab.com/juliendehos/neurk/badges/master/build.svg)](https://gitlab.com/juliendehos/neurk/pipelines) 

This is a very basic neural network library,
initially developed for teaching purposes.

Three implementations are provided:

- nn42h: a haskell implementation
- neurk_cpp: a C++ implementation
- neurk_eigen: a C++ implementation using the [eigen](http://eigen.tuxfamily.org) library

