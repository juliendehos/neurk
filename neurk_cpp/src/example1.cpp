// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "neurk.hpp"

// example from http://www-lisic.univ-littoral.fr/~teytaud/
int main(int, char**)
{
    // initialize network
    Network network;
    // neurons
    network._neurons = std::vector<Neuron>(6);
    network._neurons[0].init(0.0);
    network._neurons[1].init(0.0);
    network._neurons[2].init(0.0);
    network._neurons[3].init(1.0);
    network._neurons[4].init(1.0);
    network._neurons[5].init(1.0);
    // synapses
    network._synapses = std::vector<Synapse>(9);
    network._synapses[0].init(&network._neurons[0], &network._neurons[3], 0.2);
    network._synapses[1].init(&network._neurons[1], &network._neurons[3], 0.1);
    network._synapses[2].init(&network._neurons[2], &network._neurons[3], 0.3);
    network._synapses[3].init(&network._neurons[0], &network._neurons[4], -0.3);
    network._synapses[4].init(&network._neurons[1], &network._neurons[4], -0.2);
    network._synapses[5].init(&network._neurons[2], &network._neurons[4], 0.4);
    network._synapses[6].init(&network._neurons[0], &network._neurons[5], 0.4);
    network._synapses[7].init(&network._neurons[3], &network._neurons[5], 0.5);
    network._synapses[8].init(&network._neurons[4], &network._neurons[5], -0.4);

    // inputs/outputs
    network._ptrInputNeurons = {&network._neurons[0], &network._neurons[1], &network._neurons[2]};
    network._ptrOutputNeurons = {&network._neurons[5]};

    // evaluate network
    std::cout << "------------ evaluation ------------\n";
    network.computeValue({1, 1, 1});
    std::cout << network << std::endl;

    // train network
    std::cout << "------------ training ------------\n";
    network.computeTraining(1, {1, 1, 1}, {0});
    std::cout << network << std::endl;

    // evaluate network
    std::cout << "------------ evaluation ------------\n";
    network.computeValue({1, 1, 1});
    std::cout << network << std::endl;

    return 0;
}

