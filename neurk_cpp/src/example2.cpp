// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "neurk.hpp"

// reseau multi-couche 4 entrees 2 sorties
// apparemment ca bug pas mais il faudrait tester plus serieusement
int main(int, char**)
{
    // initialize network
    Network network;
    network.createMultiLayer(4, 2, 1, 1);
    network.getMultiLayerNeuron(2)._parameter = 0.5;
    network.getMultiLayerSynapse(4, 5, 8)._weight = 0.2;

    // train network
    std::cout << "------------ training ------------\n";
    network.computeTraining(1, {1, 1, 1, 1}, {0, 1});
    std::cout << network << std::endl;

    // evaluate network
    std::cout << "------------ evaluation ------------\n";
    network.computeValue({1, 1, 1, 1});
    std::cout << network << std::endl;

    return 0;
}

