// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include <iostream>
#include <vector>

struct Neuron;

struct Synapse
{
    Neuron * _ptrFromNeuron;
    Neuron * _ptrToNeuron;
    double _weight;

    void init(Neuron * ptrFromNeuron, Neuron * ptrToNeuron, double weight);
};

struct Neuron
{
    std::vector<Synapse*> _ptrInputSynapses;
    std::vector<Synapse*> _ptrOutputSynapses;
    
    double _parameter;
    double _value;
 
    double _trainingError;
    double _trainingValue;

    void init(double parameter);

    double sigmoid(double x) const;

    void computeLocalValue();
    void computeLocalTraining();    
};

struct Network
{
    std::vector<Neuron> _neurons;
    std::vector<Synapse> _synapses;

    std::vector<Neuron *> _ptrInputNeurons;
    std::vector<Neuron *> _ptrOutputNeurons;

    void computeTraining(double alpha, 
            const std::vector<double> & data,
            const std::vector<double> & knowledge);
    void computeValue(const std::vector<double> & data);

    // multi-layer neural network (M inputs, N outputs)
    void createMultiLayer(int M, int N, double weight, double parameter);
    Neuron & getMultiLayerNeuron(int i);
    Synapse & getMultiLayerSynapse(int M, int i, int j);
};

std::ostream & operator<<(std::ostream & os, const Network & n);

