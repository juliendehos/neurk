// Copyright © 2015 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "neurk.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <set>
#include <numeric>

//////////////////////////////////////////////////////////////////////
// Synapse
//////////////////////////////////////////////////////////////////////

void Synapse::init(Neuron * ptrFromNeuron, Neuron * ptrToNeuron, double weight) 
{
    // update members
    _ptrFromNeuron = ptrFromNeuron;
    _ptrToNeuron = ptrToNeuron;
    _weight = weight;

    // add the synapse in the two neurons
    _ptrFromNeuron->_ptrOutputSynapses.push_back(this);
    _ptrToNeuron->_ptrInputSynapses.push_back(this);
}

//////////////////////////////////////////////////////////////////////
// Neuron
//////////////////////////////////////////////////////////////////////

double Neuron::sigmoid(double x) const
{
    return 1.0 / ( 1.0 + exp(- x*_parameter) );
}

void Neuron::init(double parameter) 
{
    _parameter = parameter;
}

void Neuron::computeLocalValue()
{
    // if no input neuron, it is a data neuron and nothing to do
    // else update output value from input neurons
    if (not _ptrInputSynapses.empty())
    {
        double sigma = std::accumulate(_ptrInputSynapses.begin(),
                _ptrInputSynapses.end(), 0.0, 
                [](double a, const Synapse * s)
                {return a + s->_ptrFromNeuron->_value * s->_weight;});
        _value = sigmoid(sigma);
    }
}

void Neuron::computeLocalTraining() 
{
    // if output neuron 
    if (_ptrOutputSynapses.empty())
    {
        _trainingError = (_trainingValue - _value) * _value * (1 - _value);
    }
    else
    {
        double v = std::accumulate(_ptrOutputSynapses.begin(),
                _ptrOutputSynapses.end(), 0.0,
                [](double a, const Synapse * s)
                { return a + s->_ptrToNeuron->_trainingError * s->_weight; });
        _trainingError = v * _value * (1 - _value);
    }
}

std::ostream & operator<<(std::ostream & os, const Network & n)
{
    os << "neuron; parameter; value \n";
    for (const Neuron & l : n._neurons)
        os << "  " << std::addressof(l) << "; " << l._parameter << "; " << l._value << std::endl;
    os << "synapse; from neuron; to neuron; weight\n";
    for (const Synapse & s : n._synapses)
        os << "  " << std::addressof(s) << "; " << s._ptrFromNeuron 
            << "; " << s._ptrToNeuron << "; " << s._weight << std::endl;
    os << "output neuron; value\n";
    for (const Neuron * l : n._ptrOutputNeurons)
        os << "  " << l << "; " << l->_value << std::endl;
    return os;
}

//////////////////////////////////////////////////////////////////////
// Network
//////////////////////////////////////////////////////////////////////

void Network::computeTraining(double alpha, 
        const std::vector<double> & data,
        const std::vector<double> & knowledge)
{
    assert(_ptrOutputNeurons.size() == knowledge.size());

    // evaluate network
    computeValue(data);

    // compute training errors of all neurons
    std::set<Neuron*> set1;
    std::set<Neuron*> set2;
    std::set<Neuron*> * ptrCurrentSet = &set1;
    std::set<Neuron*> * ptrNextSet = &set2;
 
    // write knowledge in output neurons
    // for each output neuron
    for (unsigned i=0; i<_ptrOutputNeurons.size(); i++)
    {
        // write knowledge in output neurons
        _ptrOutputNeurons[i]->_trainingValue = knowledge[i];

        // remember the neuron has to be updated
        set1.insert(_ptrOutputNeurons[i]);
    }

    // update neurons using multiple passes
    // from output neurons to input neurons
    while (not ptrCurrentSet->empty())
    {
        // for each neuron of the set to update
        for (Neuron * ptrNeuron : *ptrCurrentSet)
        {
            // update the neuron value 
            ptrNeuron->computeLocalTraining();

            // get input neurons for the next updating pass
            for (Synapse * ptrInputSynapse : ptrNeuron->_ptrInputSynapses)
            {
                ptrNextSet->insert(ptrInputSynapse->_ptrFromNeuron);
            }
        }

        // go to next updating pass
        std::swap(ptrCurrentSet, ptrNextSet);
        ptrNextSet->clear();
    }

    // update synapse weights
    for (Synapse & s : _synapses)
    {
        s._weight += alpha * s._ptrToNeuron->_trainingError * s._ptrFromNeuron->_value;
    }
}

void Network::computeValue(const std::vector<double> & data)
{
    assert(_ptrInputNeurons.size() == data.size());

    std::set<Neuron*> set1;
    std::set<Neuron*> set2;
    std::set<Neuron*> * ptrCurrentSet = &set1;
    std::set<Neuron*> * ptrNextSet = &set2;
    
    // for each input neuron
    for (unsigned i=0; i<_ptrInputNeurons.size(); i++)
    {
        // put data into the neuron
        _ptrInputNeurons[i]->_value = data[i];

        // remember the neuron has to be updated
        set1.insert(_ptrInputNeurons[i]);
    }

    // update neurons using multiple passes
    // from input neurons to output neurons
    while (not ptrCurrentSet->empty())
    {
        // for each neuron of the set to update
        for (Neuron * ptrNeuron : *ptrCurrentSet)
        {
            // update the neuron value 
            ptrNeuron->computeLocalValue();

            // get output neurons for the next updating pass
            for (Synapse * ptrOutputSynapse : ptrNeuron->_ptrOutputSynapses)
            {
                ptrNextSet->insert(ptrOutputSynapse->_ptrToNeuron);
            }
        }

        // go to next updating pass
        std::swap(ptrCurrentSet, ptrNextSet);
        ptrNextSet->clear();
    }
}

void Network::createMultiLayer(int M, int N, double weight, double parameter)
{
    // clear previous network
    _synapses.clear();
    _neurons.clear();
    _ptrInputNeurons.clear();
    _ptrOutputNeurons.clear();

    // reserve memory to store all neurons and synapses
    // otherwise reallocation and invalid pointers
    _neurons.reserve(M*M);
    _synapses.reserve(M*M*M);


    // add input neurons
    for (int i=0; i<M; i++)
    {
        _neurons.push_back(Neuron());
        _neurons[i].init(parameter);
        _ptrInputNeurons.push_back(&_neurons[i]);
    }

    // add other neurons and add synapse
    // for all layer
    for (int i=M,k=0; i>N; i--)
    {
        // for all neurons of the sub-layer
        for (int j=0; j<i-1; j++)
        {
            // add a neuron
            _neurons.push_back(Neuron());
            _neurons.back().init(parameter);

            // add synapses from neurons of the main layer to the new neuron
            for (int l=0; l<i; l++)
            {
                _synapses.push_back(Synapse());
                _synapses.back().init(&_neurons[k+l], &_neurons.back(), weight);
            }
        }
        k += i;
    }

    // get output neurons
    for (int i=0; i<N; i++)
    {
        _ptrOutputNeurons.push_back(&_neurons[_neurons.size()-N+i]);
    }
}

Neuron & Network::getMultiLayerNeuron(int i)
{
    return _neurons[i];
}

Synapse & Network::getMultiLayerSynapse(int M, int i, int j)
{
    int iN = 0;
    int iS = 0;
    int nL = M;
    while (iN < i-nL)
    {
        iN += nL;
        iS += nL * (nL - 1);
        nL--;
    }
    iS += (i - iN) * (nL - 1) + j - (iN + nL);
    return _synapses[iS];
}

